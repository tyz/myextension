PHP_ARG_ENABLE(myext,
  [Whether to enable the "myext" extension],
  [  enable-myext        Enable "myext" extension support])

if test $PHP_MYEXT != "no"; then
  PHP_SUBST(MYEXT_SHARED_LIBADD)
  PHP_NEW_EXTENSION(myext, myext.c, $ext_shared)
fi