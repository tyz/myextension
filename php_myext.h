#ifndef PHP_MYEXT_H
#define PHP_MYEXT_H 1

#define PHP_MYEXT_VERSION "0.0.4-test"
#define PHP_MYEXT_EXTNAME "myext"

/* Import configure options
   when building outside of
   the PHP source tree */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

/* Include PHP Standard Header */
#include "php.h"

extern zend_module_entry myext_module_entry;
#define phpext_myext_ptr &myext_module_entry

#endif
