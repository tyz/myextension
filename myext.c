#include "php_myext.h"

PHP_FUNCTION(hello_world) {
    php_printf("Hello World!\n");
}

PHP_FUNCTION(hello_long){
    RETVAL_LONG(42);
    return;
}

PHP_FUNCTION(myext_array_range)
{
    if (return_value_used) {
        int i;
        /* Return an array from 0 - 999 */
        array_init(return_value);
        for(i = 0; i < 1000; i++) {
            add_next_index_long(return_value, i);
        }
        return;
    } else {
        /* Save yourself the effort */
        php_error_docref(NULL TSRMLS_CC, E_NOTICE,
               "Static return-only function called without processing output");
        RETURN_NULL();
    }
}


static zend_function_entry myext_functions[] = {
    PHP_FE(hello_world,        NULL)
    PHP_FE(hello_long,		   NULL)
    PHP_FE(myext_array_range,  NULL)
    { NULL, NULL, NULL }
};

zend_module_entry myext_module_entry = {
#if ZEND_MODULE_API_NO >= 20010901
  STANDARD_MODULE_HEADER,
#endif
  PHP_MYEXT_EXTNAME,
  myext_functions, // Functions
  NULL, // MINIT
  NULL, // MSHUTDOWN
  NULL, // RINIT
  NULL, // RSHUTDOWN
  NULL, // MINFO
#if ZEND_MODULE_API_NO >= 20010901
  PHP_MYEXT_VERSION,
#endif
  STANDARD_MODULE_PROPERTIES
};

#ifdef COMPILE_DL_MYEXT
ZEND_GET_MODULE(myext)
#endif
